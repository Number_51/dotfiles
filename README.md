This is my stuff. I make no claim and accept no responsibility for any of the code here, or the use of said code. To the best of my knowledge, everything is licensed under the GPL, BSD, or other such permissive license. If anything is in question or otherwise licensed, let me know and I'll remove it. Anything written by myself is hereby free to use and given the GPLv3 license. I don't claim that anything I've written was done entirely by myself. I build on the shoulders of others who know far better, and adapt code with permissive license for my own use.

I am an amateur. I sometimes do things in a very convoluted way (scripts calling other scripts); there is always room for improvement and I treat it as a learning experience. My code may be messy, have testing and debugging code left in (but commented out for the most part), and my documentation is not great (hopefully not too bad either).

I am an amateur. I sometimes do things in a non-standard way. I usually have no idea what the standards for certain things are and just do them to suit myself (i.e. my keybindings are a mess and are probably not what you're used to). Take it and make it your own.

The shell scripts, executables, and other files in the "scripts" directory are normally located in my ~/bin directory (/usr/home/username/bin or more recently /home/username/bin on FreeBSD). Put them where you like and edit scripts and configs to point to them. If you use my files and configs you may have to go through everything and change things like /usr/home/username, replacing username with your own username. Paths may need to be edited and permissions changed to get things to work for you on your system. All of this was done for my own use so I didn't think anything of using absolute paths rather than $HOME, $USER, and/or other XDG specs. I did recently go back through a lot of the code here and replace absolute paths with ~ and/or $HOME, but I'd still read through everything if I were you.

Some of the small command line utilities I use (and may be called from scripts and other programs) are:
* xprop/xdotool
* htop/btop
* neofetch
* dunst
* rofi/dzen/dmenu
* clipmenu
* weather
* gcalcli
* play/aplay/paplay
* figlet/lolcat

Some cron jobs I have set up:
```
*/10 6-16 * * 1-5 /usr/local/bin/gcalcli remind
15 */2 * * 1-5 /usr/home/username/bin/need-update
55 5-15/1 * * 1-5 /usr/home/username/bin/need-weather
20,50 5-15/1 * * 1-5 /usr/home/username/bin/gcal-agenda
```

My FreeBSD/Qtile Screenshot (My current FreeBSD WM.):

![Alt FreeBSD-Qtile](https://gitlab.com/Number_51/dotfiles/-/raw/main/FreeBSD-Qtile.png)

My FreeBSD/XMonad Screenshot (This is old. Not even sure it works anymore):

![Alt FreeBSD-XMonad](https://gitlab.com/Number_51/dotfiles/-/raw/main/FreeBSD-XMonad.png)
