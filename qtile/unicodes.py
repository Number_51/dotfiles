# Not using this anymore.
# Spacers and unicode characters are defined to the main config (config.py)
from libqtile.widget.textbox import TextBox

def left_side(bg_color, fg_color):
    return TextBox(
        text='\ue0c7\ue0c7',
        padding=5,
        fontsize=18,
        background=bg_color,
        forground=fg_color)

def right_side(bg_color, fg_color):
    return TextBox(
        text='\ue0c6',
        padding=5,
        fontsize=18,
        background=bg_color,
        forground=fg_color)
