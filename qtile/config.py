# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import subprocess

from typing import List  # noqa: F401

from libqtile import bar, layout, widget, qtile, hook
from libqtile.config import Click, Drag, Group, ScratchPad, DropDown, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from libqtile.layout.columns import Columns

@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.Popen([home]).wait()

@hook.subscribe.startup
def runner():
    subprocess.Popen(['xsetroot', '-cursor_name', 'left_ptr']).wait()

class ColumnOffset(Columns):
    def __init__(self, margin=0, reserved_width=310, **config):
        super().__init__(margin=margin, **config)
        self.reserved_width = reserved_width

@hook.subscribe.layout_change
def layout_change(layout, group):
    if layout.name in ["max", "monadtall", "monadwide", "treetab"]:
        gap_size_top = 0
        gap_size_right = 0
        gap_size_bottom = 0
        gap_size_left = 0
    elif layout.name == "columnoffset":
        gap_size_top = 4
        gap_size_right = 4
        gap_size_bottom = 4
        gap_size_left = 310
    else:
        gap_size_top = 4
        gap_size_right = 4
        gap_size_bottom = 4
        gap_size_left = 4

    if len(group.qtile.screens) > 0:
        group.qtile.screens[0].top = bar.Gap(gap_size_top + 26)
        group.qtile.screens[0].right = bar.Gap(gap_size_right)
        group.qtile.screens[0].bottom = bar.Gap(gap_size_bottom)
        group.qtile.screens[0].left = bar.Gap(gap_size_left)

    group.layout_all()
#    group.qtile.cmd_refresh()

mod = "mod4"
terminal = guess_terminal()

keys = [
    # Switch between windows.
    Key([mod], "h", lazy.layout.left(),
        desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(),
        desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(),
        desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(),
        desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(),
        desc="Move focus to next window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(),
        desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(),
        desc="Grow window up"),

    Key([mod], "n", lazy.layout.normalize(),
        desc="Reset all window sizes"),
    Key([mod], "f", lazy.window.toggle_fullscreen(),
        desc="Toggle fullscreen"),
    Key([mod], "m", lazy.window.toggle_maximize(),
        desc="Toggle maximize"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed.
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes.
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),

    # Spawn a terminal.
    Key([mod], "Return", lazy.spawn(terminal),
        desc="Launch terminal"),

    # Toggle between different layouts as defined below.
    Key([mod], "Tab", lazy.next_layout(),
        desc="Toggle between layouts"),

    # Kill the focused window.
    Key([mod, "shift"], "c", lazy.window.kill(),
        desc="Kill focused window"),

    # Restart or shutdown Qtile.
    Key([mod, "control"], "r", lazy.restart(),
        desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(),
        desc="Shutdown Qtile"),

    # Prompt widget/spawn a commad.
    Key([mod], "r", lazy.spawncmd(),
        desc="Spawn a command using a prompt widget"),

    # Printscreen/Screenshot.
    Key([], "Print", lazy.spawn(os.path.expanduser('~/bin/screenshot-full'), shell=True)),
    Key([mod, "control"], "Print", lazy.spawn(os.path.expanduser('~/bin/screenshot-win'), shell=True)),
    Key([mod, "shift"], "Print", lazy.spawn(os.path.expanduser('~/bin/screenshot-rect'), shell=True)),

    # ScratchPads.
    Key([], "F12", lazy.group['0'].dropdown_toggle('alacritty')),

    # Notifications.
    Key([mod], "grave", lazy.spawn("dunstctl history-pop"), desc="Show dunst notification history"),
    Key([mod, "shift"], "n", lazy.spawn(os.path.expanduser('~/bin/nowplaying_deadbeef')), desc="Show 'Now Playing in DeaDBeeF' notification"),
]

# groups = [Group(i) for i in "123456789"]

groups = [
    Group("1", label="main"),
    Group("2", label="www"),
    Group("3", label="sys"),
    Group("4", label="dev"),
    Group("5", label="doc"),
    Group("6", label="aud"),
    Group("7", label="vid"),
    Group("8", label="gfx"),
    Group("9", label="tmp"),
    ScratchPad("0", [
        DropDown("alacritty", "alacritty", width=0.8, height=0.8, x=0.1, y=0.1, opacity=1.0, on_focus_lost_hide=False),
    ]),
]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),

        # mod1 + shift + letter of group = switch to & move focused window to group
        # Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True),
        #     desc="Switch to & move focused window to group {}".format(i.name)),
        # Or, use below if you prefer not to switch to that group.
        # # mod1 + shift + letter of group = move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            desc="move focused window to group {}".format(i.name)),
    ])

layouts = [
    layout.Columns(
        border_focus='#ff0000',
        border_normal='#bbbbbb',
        border_width=2,
        margin=4
    ),
    ColumnOffset(
        border_focus='#ff0000',
        border_normal='#bbbbbb',
        border_width=2,
        margin=4
    ),
    layout.Max(
        margin=8
    ),
    layout.Stack(
        num_stacks=2,
        border_focus='#ff0000',
        border_normal='#bbbbbb',
        border_width=2,
        margin=4
    ),
    layout.Bsp(
        border_focus='#ff0000',
        border_normal='#bbbbbb',
        border_width=2,
        margin=4
    ),
    layout.Matrix(
        border_focus='#ff0000',
        border_normal='#bbbbbb',
        border_width=2,
        margin=4
    ),
    layout.MonadTall(
        border_focus='#ff0000',
        border_normal='#bbbbbb',
        border_width=2,
        margin=8
    ),
    layout.MonadWide(
        border_focus='#ff0000',
        border_normal='#bbbbbb',
        border_width=2,
        margin=8
    ),
    layout.RatioTile(
        border_focus='#ff0000',
        border_normal='#bbbbbb',
        border_width=2,
        margin=4
    ),
    layout.Tile(
        border_focus='#ff0000',
        border_normal='#bbbbbb',
        border_width=2,
        margin=4
    ),
    layout.TreeTab(
        active_bg='#ff0000',
        active_fg='#bbbbbb',
        border_width=2
    ),
]

widget_defaults = dict(
    font='NotoSans, NotoSans Nerd Font',
    fontsize=13,
    padding=3,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Sep(linewidth=0, padding=6),
                # widget.TextBox("", mouse_callbacks={'Button1': lambda: qtile.cmd_spawn("rofi -show drun -theme ~/.config/rofi/launchpad.rasi"),},),
                widget.Image(filename="~/Pictures/UIElements/Launcher.png", scale=True, margin=1, mouse_callbacks={'Button1': lambda: qtile.cmd_spawn("rofi -show drun -theme ~/.config/rofi/launchpad.rasi"),},),
                widget.Sep(linewidth=0, padding=6),
                widget.Prompt(foreground="#ff0000", background="#272727", prompt=" RUN: ", fmt="<b>{}</b>"),
                widget.GroupBox(
                    borderwidth=2,
                    active="#f8f8f8",
                    inactive="#8c8c8c",
                    rounded=False,
                    highlight_method="line",
                    highlight_color="#272727",
                    this_current_screen_border="#ff0000",
                    this_screen_border="#ffa600",
                ),
                #widget.Sep(linewidth=0, padding=6),
                widget.TextBox("", font='FontAwesome', fontsize=23, foreground="#272727", padding=6),
                # left_side("#000000", "#FFFFFF"),
                widget.CurrentLayout(fmt="[ {} ]", background="#272727"),
                widget.WindowCount(fmt="[ {} ]", background="#272727"),
                # right_side("#000000", "#FFFFFF"),
                widget.TextBox("", font='FontAwesome', fontsize=23, foreground="#272727", padding=0),
                widget.Sep(linewidth=0, padding=18),
                widget.TaskList(
                    icon_size=16,
                    borderwidth=2,
                    border=["#232323", "#1f1f1f", "#191919", "#141414", "#0e0e0e", "#080808", "#030303", "#ffa600"],
                    unfocused_border=["#232323", "#1f1f1f", "#191919", "#141414", "#0e0e0e", "#080808", "#030303", "#bbbbbb"],
                    urgent_border="#ff0000",
                    rounded=False,
                    margin=1,
                    padding_x=6,
                    spacing=6,
                    highlight_method="border",
                    txt_floating=" 󰖲 ",
                    txt_maximized=" 󰖯 ",
                    txt_minimized=" 󰖰 ",
                ),
                widget.Chord(
                    chords_colors={
                        'launch': ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.Sep(linewidth=0, padding=24),
                widget.OpenWeather(
                    cityid="5039133",
                    format="  Temp: {main_temp:.0f}°{units_temperature}",
                    metric=False,
                    update_interval=300,
                    mouse_callbacks={'Button1': lambda: qtile.cmd_spawn("dunst-wth"),},
                ),
                #widget.Sep(linewidth=0, padding=6),
                widget.TextBox("", font='FontAwesome', fontsize=23, foreground="#272727", padding=6),
                widget.TextBox("󰗶", fontsize=14, foreground="#3daae9", background="#272727", padding=4, mouse_callbacks={'Button1': lambda: qtile.cmd_spawn("dunst-upt"),},),
                widget.TextBox("", foreground="#ffffff", background="#272727", padding=4, mouse_callbacks={'Button1': lambda: subprocess.run(['xdotool', 'key', 'F12']),},),
                widget.TextBox("󰮫", fontsize=16, foreground="#00ff00", background="#272727", padding=4, mouse_callbacks={'Button1': lambda: qtile.cmd_spawn("rofi -show drun -theme ~/.config/rofi/quickdock.rasi"),},),
                widget.TextBox("󱘢", foreground="#ffffff", background="#272727", padding=4, mouse_callbacks={'Button1': lambda: qtile.cmd_spawn("clipmenu -theme ~/.config/rofi/clipmenu.rasi"),},),
                widget.TextBox("󰣠", foreground="#ff0000", background="#272727", padding=4, mouse_callbacks={'Button1': lambda: qtile.cmd_spawn("dunst-upd"),},),
                widget.TextBox("", font='FontAwesome', fontsize=23, foreground="#272727", padding=0),
                widget.Sep(linewidth=0, padding=8),
                # widget.Systray(),
                widget.Clock(format="   %A, %B %d, %Y", mouse_callbacks={'Button1': lambda: qtile.cmd_spawn("dunst-cal"),},),
                #widget.Sep(linewidth=0, padding=6),
                widget.TextBox("", font='FontAwesome', fontsize=23, foreground="#272727", padding=6),
                widget.Clock(format="   %I:%M %p %Z ", background="#272727", padding=0, mouse_callbacks={'Button1': lambda: qtile.cmd_spawn("dunst-agn"),},),
                widget.TextBox("", font='FontAwesome', fontsize=23, foreground="#272727", padding=0),
                widget.Sep(linewidth=0, padding=8),
                widget.TextBox("", fontsize=14, foreground="#3daae9", padding=4, mouse_callbacks={'Button1': lambda: qtile.cmd_spawn("slock"),},),
                widget.QuickExit(default_text="", fontsize=14, countdown_format="<b>{}s</b>", countdown_start=3, foreground="#ff0000", padding=4),
                widget.Sep(linewidth=0, padding=6),
            ],
            size=26,
            background=["#000000", "#272727"],
            margin=[0, 0, 4, 0],
        ),
    right=bar.Gap(4),
    bottom=bar.Gap(4),
    left=bar.Gap(4)
    ),
]

# Floating layout mouse actions.
mouse = [
    Drag([mod, "shift"], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod, "shift"], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod, "shift"], "Button2", lazy.window.bring_to_front()),
    Click([mod, "control"], "Button3", lazy.spawn("window-menu"))
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class='confirmreset'),  # gitk
        Match(wm_class='makebranch'),  # gitk
        Match(wm_class='maketag'),  # gitk
        Match(wm_class='kshutdown'),  # kshutdown
        Match(wm_class='ssh-askpass'),  # ssh-askpass
        Match(role='messageview'),  # claws-mail message view
        Match(title='branchdialog'),  # gitk
        Match(title='pinentry'),  # GPG key password entry
    ],
    border_width=2,
    border_focus="#ff0000",
    border_normal="#bbbbbb",
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
