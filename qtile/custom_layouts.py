# Not using this anymore.
# The "columnoffset" custom layout is defined to the main config (config.py)
from libqtile.layout.columns import Columns

class ReservedLeftColumns(Columns):
    def __init__(self, reserved_width=305, **config):
        self.reserved_width = reserved_width
        super().__init__(**config)

    def configure(self, client, screen_rect):
        if client.has_focus:
            border_color = self.group.qtile.color("focus")
        else:
            border_color = self.group.qtile.color("normal")

        # Adjust screen_rect to reserve the left area
        screen_rect.width -= self.reserved_width
        screen_rect.x += self.reserved_width

        super().configure(client, screen_rect)
