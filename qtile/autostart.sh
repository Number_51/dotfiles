#!/usr/local/bin/bash

#picom &
compton --config ~/.config/compton/compton.conf &
#dunst >> /home/bbell/.config/dunst/dunst.log 2>&1 &
dunst &
clipmenud &
nitrogen --restore &
conky -d &
alacritty -e btop &
