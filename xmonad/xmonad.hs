  -- Base
import XMonad
import XMonad.ManageHook
import XMonad.Operations
import qualified XMonad.StackSet as W

  -- Actions
import XMonad.Actions.CycleWS
import XMonad.Actions.FloatSnap
import XMonad.Actions.MouseResize
import XMonad.Actions.WindowBringer
import qualified XMonad.Actions.FlexibleResize as Flex

  -- Data
import Data.Char (isSpace, toUpper, toLower)
import Data.Maybe (fromJust)
import Data.Monoid
import Data.Maybe (isJust)
import Data.Tree
import qualified Data.Map as M

  -- Hooks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP

  -- Layouts
import XMonad.Layout.Decoration
import XMonad.Layout.Gaps
import XMonad.Layout.Grid
import XMonad.Layout.Magnifier
import XMonad.Layout.Renamed
import XMonad.Layout.ResizableTile
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spacing
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns
import XMonad.Layout.WindowArranger

  -- Layouts Modifiers
import XMonad.Layout.LayoutModifier
import XMonad.Layout.ShowWName

  -- Utilities
import XMonad.Util.ClickableWorkspaces
import XMonad.Util.Dmenu
import XMonad.Util.EZConfig
import XMonad.Util.Loggers
import XMonad.Util.NamedScratchpad
import XMonad.Util.SpawnOnce
import XMonad.Util.Ungrab

myModMask :: KeyMask
myModMask = mod4Mask

myTerminal :: String
myTerminal = "alacritty"

myBrowser :: String
myBrowser = "chrome"

myMailClient :: String
myMailClient = "claws-mail"

myPlayer :: String
myPlayer = "deadbeef"  -- | WM_CLASS(STRING) = "deadbeef", "Deadbeef"

myPlayerClass :: String
myPlayerClass = "Deadbeef"  -- | WM_CLASS(STRING) = "deadbeef", "Deadbeef"

myFileMan :: String
myFileMan = "pcmanfm"

myWorkspaces = ["main", "www", "sys", "dev", "doc", "aud", "vid", "gfx", "tmp"]
myWorkspaceIndices = M.fromList $ zipWith (,) myWorkspaces [1..]

myWindowCount :: X (Maybe String)
myWindowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
  { swn_font    = "xft:Inconsolata:pixelsize=60:bold:antialias=true:hinting=true"
  , swn_fade    = 2.0
  , swn_bgcolor = "#1c1f24"
  , swn_color   = "#ee9a00"
  }

myMouseBindings :: XConfig Layout -> M.Map (ButtonMask, Button) (Window -> X ())
myMouseBindings (XConfig {XMonad.modMask = modMask}) = M.fromList $
    [ ((modMask,               button1), (\w -> focus w >> mouseMoveWindow w
                                                        >> afterDrag (snapMagicMove (Just 20) (Just 20) w)))
    , ((modMask .|. shiftMask, button1), (\w -> focus w >> mouseMoveWindow w
                                                        >> afterDrag (snapMagicResize [L,R,U,D] (Just 20) (Just 20) w)))
    , ((modMask,               button2), (\w -> focus w >> windows W.swapMaster))
    , ((modMask .|. shiftMask, button2), (\w -> focus w >> windows W.swapMaster))
    , ((modMask              , button3), (\w -> focus w >> Flex.mouseResizeWindow w))
    , ((modMask .|. shiftMask, button3), (\w -> focus w >> Flex.mouseResizeWindow w))
    ]

myScratchPads :: [NamedScratchpad]
myScratchPads = [ NS "myTerminalSP" spawnTerm findTerm manageTerm
                , NS "myCalculatorSP" spawnCalc findCalc manageCalc
                , NS "myMusicPlayerSP" spawnAud findAud manageAud
                ]
  where
    spawnTerm  = myTerminal ++ " -t Alacritty[NSP]"
    findTerm   = title =? "Alacritty[NSP]"
    manageTerm = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w
    spawnCalc  = "kcalc"
    findCalc   = className =? "kcalc"
    manageCalc = customFloating $ W.RationalRect l t w h
               where
                 h = 0.5
                 w = 0.4
                 t = 0.75 -h
                 l = 0.70 -w
    spawnAud   = myPlayer
    findAud    = className =? myPlayerClass
    manageAud  = customFloating $ W.RationalRect l t w h
               where
                 h = 0.5
                 w = 0.5
                 t = 0.75 -h
                 l = 0.75 -w

-- myManageGimp :: ManageHook
-- myManageGimp = className =? "Gimp" --> (doShift "gfx") <+> composeOne
--     [ stringProperty "WM_WINDOW_ROLE" =? "gimp-image-window" -?> doCenterFloat
--     , stringProperty "WM_WINDOW_ROLE" =? "gimp-image-new"    -?> doCenterFloat
--     , stringProperty "WM_WINDOW_ROLE" =? "gimp-toolbox"      -?> doFloat
--     , stringProperty "WM_WINDOW_ROLE" =? "gimp-dock"         -?> doFloat
--     , return True -?> doFloat
--     ]

myStartupHook :: X ()
myStartupHook = do
  spawn "killall conky"
  spawn "killall trayer"

  spawnOnce "compton -b --config /usr/home/username/.config/compton/compton.conf"
  spawnOnce "dunst"
  spawnOnce "clipmenud"
  spawnOnce "xsetroot -cursor_name left_ptr"
  spawnOnce "nitrogen --restore"
  -- spawnOnce "feh --image-bg black --bg-center '/usr/home/username/Pictures/Wallpaper/CpW0Mz.jpg'"
  spawnOnce "xscreensaver -no-splash"
  -- spawnOnce "xautolock -time 20 -locker slock"

  spawn ("sleep 2 && conky -d")
  spawn ("sleep 2 && trayer --edge top --align right --distance 2 --distancefrom top --SetDockType true --SetPartialStrut true --expand true --height 16 --transparent true --alpha 0 --tint 0x1d1f21 --widthtype request --padding 8")

main :: IO ()
main = xmonad
     . ewmhFullscreen
     . ewmh
     . withEasySB (statusBarProp "xmobar ~/.config/xmobar/xmobar.conf" (clickablePP (filterOutWsPP [scratchpadWorkspaceTag] myXmobarPP))) toggleStrutsKey -- | replace clickablePP with pure to make xmobar unclickable
     $ myConfig
  where
    toggleStrutsKey :: XConfig Layout -> (KeyMask, KeySym)
    toggleStrutsKey XConfig{ modMask = m } = (m, xK_s)

myConfig = def
    { modMask       = myModMask     -- | Rebind Mod to the 'myModMask' key
    , terminal      = myTerminal 
    , startupHook   = myStartupHook
    , workspaces    = myWorkspaces
    , borderWidth   = 2
    , mouseBindings = myMouseBindings
    , layoutHook    = renamed [CutWordsLeft 1]
                    $ spacingRaw False (Border 4 4 4 4) True (Border 4 4 4 4) True
                    $ mouseResize
                    $ windowArrange
                    $ myLayoutHook  -- | Use custom layouts
    , manageHook    = myManageHook  -- | Match on certain windows
    }
  `additionalKeysP`
    [ ("M-p"             , spawn "dmenu_run -i -p 'RUN:' -fn 'Inconsolata:bold:size=10' -nb '#1d1f21' -sb '#a60303'")
    , ("M-b"             , spawn (myBrowser))
    , ("M-f"             , spawn (myFileMan))
    , ("M-m"             , spawn (myMailClient))
    , ("M-a"             , spawn (myPlayer))
    , ("M-g"             , spawn "gimp")
    , ("M-M1-b"          , spawn (myTerminal ++ " -e btop"))
    , ("M-M1-h"          , spawn (myTerminal ++ " -e htop"))
    , ("M-M1-s"          , spawn "xscreensaver-command --activate")
    , ("M-M1-l"          , spawn "slock")
    , ("<Print>"         , unGrab *> spawn "scrot")
    , ("S-<Print>"       , unGrab *> spawn "scrot -s")
    , ("M-<Up>"          , windows W.focusUp)
    , ("M-<Down>"        , windows W.focusDown)
    , ("M-<Left>"        , prevWS)
    , ("M-<Right>"       , nextWS)
    , ("M-S-a"           , sendMessage MirrorShrink)
    , ("M-S-z"           , sendMessage MirrorExpand)
    , ("M-S-q"           , spawn "byebye-exe")
    , ("M-S-g"           , gotoMenuArgs ["-i", "-p", "Go To:", "-fn", "Inconsolata:bold:size=10", "-nb", "#1d1f21", "-sb", "#a60303"])
    , ("M-S-b"           , bringMenuArgs ["-i", "-p", "Bring:", "-fn", "Inconsolata:bold:size=10", "-nb", "#1d1f21", "-sb", "#a60303"])
    , ("<F12>"           , namedScratchpadAction myScratchPads "myTerminalSP")
    , ("M-M1-t"          , namedScratchpadAction myScratchPads "myTerminalSP")
    , ("M-M1-c"          , namedScratchpadAction myScratchPads "myCalculatorSP")
    , ("M-M1-a"          , namedScratchpadAction myScratchPads "myMusicPlayerSP")
    , ("M-S-d"           , withFocused $ toggleDynamicNSP "DSP")
    , ("M-M1-d"          , dynamicNSPAction "DSP")
    , ("C-`"             , spawn "dunstctl history-pop")
    , ("C-<Space>"       , spawn "dunstctl close")
    , ("C-S-<Space>"     , spawn "dunstctl close-all")
    , ("C-S-<Home>"      , spawn "/home/username/bin/window-center")
    , ("C-S-<Page_Up>"   , spawn "/home/username/bin/window-upperleft")
    , ("C-S-<Page_Down>" , spawn "/home/username/bin/window-lowerright")
    ]

myManageHook :: ManageHook
myManageHook = composeAll
    [ isDialog                          --> doCenterFloat
    , className =? "confirm"            --> doCenterFloat
    , className =? "file_progress"      --> doFloat
    , className =? "dialog"             --> doCenterFloat
    , className =? "download"           --> doFloat
    , className =? "error"              --> doFloat
    , className =? "vlc"                --> doCenterFloat
    , className =? "qutebrowser"        --> doCenterFloat
    , className =? "kcalc"              --> doCenterFloat
    , className =? "Gimp"               --> (doFloat <+> doShift "gfx")
    , className =? "Deadbeef"           --> doCenterFloat
    , className =? "Claws-mail"         --> doCenterFloat
    , className =? "Chromium-browser"   --> doCenterFloat
    , className =? "Audacious"          --> doCenterFloat
    , className =? "Pcmanfm"            --> doFloat
    , className =? "dolphin"            --> doFloat
    , className =? "notification"       --> doFloat
    , className =? "splash"             --> doFloat
    , className =? "toolbar"            --> doFloat
    , className =? "Gsimplecal"         --> doFloat
    , className =? "Alarm-clock-applet" --> doFloat
    ] <+> namedScratchpadManageHook myScratchPads

myLayoutHook = showWS ( tiled ||| resizable ||| wide ||| Full ||| threeCol ||| Grid ||| tabs ||| floats )
  where
    showWS      = showWName' myShowWNameTheme
    resizable   = ResizableTall nmaster (delta) (ratio) []
    threeCol    = ThreeColMid nmaster delta ratio
    tabs        = renamed [Replace "Tabbed"]
                $ tabbed shrinkText myTabConfig
    floats      = renamed [Replace "Floats"]
                $ simplestFloat
    wide        = renamed [Replace "Wide"]
                $ Mirror tiled
    tiled       = Tall nmaster delta ratio
    nmaster     = 1      -- | Default number of windows in the master pane
    ratio       = 1/2    -- | Default proportion of screen occupied by master pane
    delta       = 3/100  -- | Percent of screen to increment by when resizing panes
    myTabConfig = def
        { activeTextColor     = "#1D1F21"
        , inactiveTextColor   = "#1D1F21"
        , activeColor         = "#EE9A00"
        , inactiveColor       = "#D47400"
        , activeBorderColor   = "#F8F8F8"
        , inactiveBorderColor = "#BBBBBB"
        , fontName            = "xft:Inconsolata:size=10:bold:antialias=true:hinting=true"
        }

myXmobarPP :: PP
myXmobarPP = def
    { ppSep             = lightgray " | "
    , ppTitleSanitize   = xmobarStrip
    , ppCurrent         = white . wrap " " "" . xmobarBorder "Bottom" "#ee9a00" 2
    , ppHidden          = white . wrap " " "" . xmobarBorder "Top" "#ff0000" 2
    , ppHiddenNoWindows = lightgray . wrap " " ""
    , ppUrgent          = red1 . wrap (yellow "!") (yellow "!")
    , ppOrder           = \[ws, l, _, wins] -> [ws, l, wins]
    -- , ppExtras          = [myWindowCount]
    , ppExtras          = [logTitles formatFocused formatUnfocused]
    }
  where
    formatFocused   = wrap (white     "[") (white     "]") . orange1 . ppWindow
    formatUnfocused = wrap (lightgray "[") (lightgray "]") . orange2 . ppWindow

    -- | Windows should have *some* title, which should not exceed a sane length.
    ppWindow :: String -> String
    ppWindow = xmobarRaw . (\w -> if null w then "untitled" else w) . shorten 30

    yellow, orange1, orange2, red1, red2, darkred1, darkred2, black, darkgray, gray, lightgray, white :: String -> String
    yellow    = xmobarColor "#FFFF00" ""
    orange1   = xmobarColor "#EE9A00" ""
    orange2   = xmobarColor "#D47400" ""
    red1      = xmobarColor "#FF0000" ""
    red2      = xmobarColor "#A60303" ""
    darkred1  = xmobarColor "#730202" ""
    darkred2  = xmobarColor "#400101" ""
    black     = xmobarColor "#000000" ""
    darkgray  = xmobarColor "#333333" ""
    gray      = xmobarColor "#8C8C8C" ""
    lightgray = xmobarColor "#BBBBBB" ""
    white     = xmobarColor "#F8F8F8" ""
