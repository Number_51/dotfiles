module Colors.BSDColors where

import Monad

colorScheme = "bsd-colors"

background = "#1D1F21"
yellow     = "#FFFF00"
orange1    = "#EE9A00"
orange2    = "#D47400"
red1       = "#FF0000"
red2       = "#A60303"
darkred1   = "#730202"
darkred2   = "#400101"
black      = "#000000"
darkgray   = "#333333"
gray       = "#8C8C8C"
lightgray  = "#BBBBBB"
white      = "#F8F8F8"
