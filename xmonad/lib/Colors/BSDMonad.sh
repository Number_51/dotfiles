# My BSD Colors
color0='#1D1F21'
color1='#FFFF00'
color2='#EE9A00'
color3='#D47400'
color4='#FF0000'
color5='#A60303'
color6='#730202'
color7='#400101'
color8='#000000'
color9='#333333'
color10='#8C8C8C'
color11='#BBBBBB'
color12='#F8F8F8'

# Extra Colors
color13='#F7CD00'
color14='#F3B400'
