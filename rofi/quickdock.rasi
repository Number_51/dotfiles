/**
 *
 * Author : Aditya Shakya (adi1090x)
 * Github : @adi1090x
 * 
 * Rofi Theme File
 * Rofi Version: 1.7.3
 **/

/*****----- Configuration -----*****/
configuration {
    modi:                       "drun";
    show-icons:                 true;
    drun-display-format:        "{name}";
}

/*****----- Global Properties -----*****/
* {
    font:                        "Noto Sans 6";
    icon-theme:                  "breeze-dark";
}

/*****----- Main Window -----*****/
window {
/*    transparency:                "real";*/
    location:                    east;
    anchor:                      east;
    fullscreen:                  false;
    width:                       100px;
    height:                      908;
    x-offset:                    0px;
    y-offset:                    0px;

    enabled:                     true;
    margin:                      0px;
    padding:                     0px;
    border:                      0px 0px 0px 0px solid;
    border-radius:               2px 0px 0px 2px;
    border-color:                #BBBBBB;
    background-color:            #1D1F21;
    cursor:                      "default";
}

/*****----- Main Box -----*****/
mainbox {
    enabled:                     true;
    spacing:                     20px;
    margin:                      0px;
    padding:                     10px 10px;
    border:                      0px solid;
    border-radius:               0px 0px 0px 0px;
    border-color:                #BBBBBB;
    background-color:            transparent;
    children:                    [ "listview" ];
}

/*****----- Listview -----*****/
listview {
    enabled:                     true;
    columns:                     1;
    lines:                       10;
    cycle:                       true;
    dynamic:                     true;
    scrollbar:                   false;
    layout:                      vertical;
    reverse:                     false;
    fixed-height:                true;
    fixed-columns:               true;
    
    spacing:                     0px;
    margin:                      0px;
    padding:                     0px;
    border:                      0px solid;
    border-radius:               0px;
    border-color:                #8B8C8C;
    background-color:            transparent;
    text-color:                  #F8F8F8;
    cursor:                      "default";
}
scrollbar {
    handle-width:                5px ;
    handle-color:                #8B8C8C;
    border-radius:               0px;
    background-color:            white /5%;
}

/*****----- Elements -----*****/
element {
    enabled:                     true;
    spacing:                     10px;
    margin:                      0px;
    padding:                     10px 10px;
    border:                      0px solid;
    border-radius:               2px;
    border-color:                #BBBBBB;
    background-color:            transparent;
    text-color:                  #F8F8F8;
    orientation:                 vertical;
    cursor:                      pointer;
}
element normal.normal {
    background-color:            transparent;
    text-color:                  #F8F8F8;
}
element selected.normal {
    border:                      0px 0px 0px 0px;
    border-radius:               2px;
    border-color:                #BBBBBB;
    background-image:            url("~/Pictures/UIElements/Quickdock-Selected.png");
    background-color:            white / 5%;
    text-color:                  #F8F8F8;
}
element-icon {
    background-color:            transparent;
    text-color:                  inherit;
    size:                        32px;
    cursor:                      inherit;
}
element-text {
    background-color:            transparent;
    text-color:                  inherit;
    highlight:                   inherit;
    cursor:                      inherit;
    vertical-align:              0.5;
    horizontal-align:            0.5;
}

/*****----- Message -----*****/
error-message {
    padding:                     20px;
    border:                      0px solid;
    border-radius:               0px;
    border-color:                #BBBBBB;
    background-color:            black / 10%;
    text-color:                  #F8F8F8;
}
textbox {
    background-color:            transparent;
    text-color:                  #F8F8F8;
    vertical-align:              0.5;
    horizontal-align:            0.0;
    highlight:                   none;
}
